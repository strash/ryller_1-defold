components {
  id: "main"
  component: "/main/main.script"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "bg_1"
  type: "sprite"
  data: "tile_set: \"/main/main.atlas\"\ndefault_animation: \"bg_1\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 205.5
    y: 411.5
    z: -1.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "bg_2"
  type: "sprite"
  data: "tile_set: \"/main/main.atlas\"\ndefault_animation: \"bg_2\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 205.5
    y: 411.5
    z: -1.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "wheel_bg"
  type: "sprite"
  data: "tile_set: \"/main/main.atlas\"\ndefault_animation: \"wheel_bg\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 205.5
    y: 548.5
    z: -0.5
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "title"
  type: "sprite"
  data: "tile_set: \"/main/main.atlas\"\ndefault_animation: \"title\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 205.5
    y: 775.5
    z: 0.5
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "page_1"
  type: "sprite"
  data: "tile_set: \"/main/main.atlas\"\ndefault_animation: \"page_active\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 117.5
    y: 145.5
    z: 1.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "page_2"
  type: "sprite"
  data: "tile_set: \"/main/main.atlas\"\ndefault_animation: \"page_inactive\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 139.5
    y: 145.5
    z: 1.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "wheel_arrow"
  type: "sprite"
  data: "tile_set: \"/main/main.atlas\"\ndefault_animation: \"wheel_arrow\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 205.5
    y: 699.5
    z: 1.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
