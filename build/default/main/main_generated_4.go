embedded_components {
  id: "btn"
  type: "sprite"
  data: "tile_set: \"/main/main.atlas\"\ndefault_animation: \"btn_1\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 124.5
    y: 52.5
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
